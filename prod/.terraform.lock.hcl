# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "2.17.0"
  constraints = "~> 2.3"
  hashes = [
    "h1:Y2YvQIMTWnl2xz82+5gOVo3zSgQFpk7o/CadXP+Ad6w=",
    "zh:05d97d21dea9acc20cb88885e658508bf23a09d2561eb203be70248f9bb7ee68",
    "zh:23ee7136382586ca1f6e5a2df6751a22e29cd4060dab0bc613b702b2edf3881c",
    "zh:2fc9647ded9459118966b6883372c4707f232220525ef56b488de738cf9c5e78",
    "zh:31624f7efb0c93461cb54f5197c6af891ba8ccb540362356b5722509a00f3976",
    "zh:3ded9720928dac2b6027e3c074511f9717726fd2e67feb17f764a3766ff22d3f",
    "zh:623c689e580dd3cd4d75a713e40583590d343ceb801e6ffbc6d90d43be85cf6f",
    "zh:63191d6bf16c9307d98ee4c31637e74687599baa05266218c5a32ed22f28b06b",
    "zh:697955f294fff567b3862e6d0e7f2bbcfcfa8bfb58be28efca2fda00d9b7ebaa",
    "zh:d4980aacac7f2e6c24e9072b284af77f0c5ad485c48a1ff041efeee3beacec58",
    "zh:e5d4e2595034fc6a7382409aef91425d8a21473532774edce60166205ee0e4af",
    "zh:f1b2f956748aba79be2f74b68035055cc99fdb6cdb2b5183e119be00c13bcda4",
  ]
}
