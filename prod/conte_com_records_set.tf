resource "cloudflare_record" "conte_com_www" {
  zone_id = cloudflare_zone.conte_com.id
  name    = "www"
  type    = "CNAME"
  ttl     = 1
  proxied = false
  value   = "ext-cust.squarespace.com"
}

resource "cloudflare_record" "conte_com_zmverify" {
  zone_id = cloudflare_zone.conte_com.id
  name    = "zb84188640"
  type    = "CNAME"
  ttl     = 1
  proxied = false
  value   = "zmverify.zoho.com"
}

resource "cloudflare_record" "conte_com_zmverify_txt" {
  zone_id = cloudflare_zone.conte_com.id
  name    = "contecommunications.com"
  type    = "TXT"
  ttl     = 1
  proxied = false
  value   = "zoho-verification=zb84188640.zmverify.zoho.com"
}

resource "cloudflare_record" "conte_com_spf" {
  zone_id = cloudflare_zone.conte_com.id
  name    = "contecommunications.com"
  type    = "TXT"
  ttl     = 1
  proxied = false
  value   = "v=spf1 include:zoho.com ~all"
}

resource "cloudflare_record" "conte_com_domainkey" {
  zone_id = cloudflare_zone.conte_com.id
  name    = "zoho._domainkey"
  type    = "TXT"
  ttl     = 1
  proxied = false
  value   = "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDsCsHO33CpTjWAfPiDgfmO9WoVR+cnmH13HpFmEOsSQLqL0UxZKEfha22aKZRchYRgIV/+anlZZkUNqBEWvD+McmdFks+UWZfpsO5q7Vug3kcg2DwTn5+F9JLUQ9YzDTXfDUGWAqdYBfm8gKv/wmaqJIduDmuV+QwsPlJqIbaY0QIDAQAB"
}

resource "cloudflare_record" "conte_com_squarespace_verify" {
  zone_id = cloudflare_zone.conte_com.id
  name    = "9by9hgcwza46czw2kttf"
  type    = "CNAME"
  ttl     = 1
  proxied = false
  value   = "verify.squarespace.com"
}

# resource "cloudflare_record" "conte_com_root_1" {
#     zone_id = "${cloudflare_zone.conte_com.id}"
#     name    = "contecommunications.com"
#     type    = "A"
#     ttl     = 1
#     proxied = false
#     value   = "198.185.159.144"
# }
# 
# resource "cloudflare_record" "conte_com_root_2" {
#     zone_id = "${cloudflare_zone.conte_com.id}"
#     name    = "contecommunications.com"
#     type    = "A"
#     ttl     = 1
#     proxied = false
#     value   = "198.185.159.145"
# }
# 
# resource "cloudflare_record" "conte_com_root_3" {
#     zone_id = "${cloudflare_zone.conte_com.id}"
#     name    = "contecommunications.com"
#     type    = "A"
#     ttl     = 1
#     proxied = false
#     value   = "198.49.23.144"
# }
# 
# resource "cloudflare_record" "conte_com_root_4" {
#     zone_id = "${cloudflare_zone.conte_com.id}"
#     name    = "contecommunications.com"
#     type    = "A"
#     ttl     = 1
#     proxied = false
#     value   = "198.49.23.144"
# }

resource "cloudflare_record" "conte_com_mx_1" {
  zone_id  = cloudflare_zone.conte_com.id
  name     = "contecommunications.com"
  type     = "MX"
  ttl      = 1
  proxied  = false
  priority = 10
  value    = "mx.zoho.com"
}

resource "cloudflare_record" "conte_com_mx_2" {
  zone_id  = cloudflare_zone.conte_com.id
  name     = "contecommunications.com"
  type     = "MX"
  ttl      = 1
  proxied  = false
  priority = 20
  value    = "mx2.zoho.com"
}

resource "cloudflare_record" "conte_com_mx_3" {
  zone_id  = cloudflare_zone.conte_com.id
  name     = "contecommunications.com"
  type     = "MX"
  ttl      = 1
  proxied  = false
  priority = 50
  value    = "mx3.zoho.com"
}

resource "cloudflare_record" "conte_com_spf_mg" {
  zone_id = cloudflare_zone.conte_com.id
  name    = "mg"
  type    = "TXT"
  value   = "v=spf1 include:mailgun.org ~all"
  ttl     = 1800
}

resource "cloudflare_record" "conte_com_mg_domainkey" {
  zone_id = cloudflare_zone.conte_com.id
  name    = "mx._domainkey.mg"
  type    = "TXT"
  value   = "k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBdIa1pVisP7yTWtpv7DMyz55wqraZp1NNmY/SPuwvnoNTxdwYbuM+fMk2DY+kycjfwdzccpVMG1r5Nr8MnLc/KApfvpJ7uzGLpkuZCUrigMH5t801KnQWY4NfG04s6bPGCxUu+d4iSTCjAl8hNohcinn5z/V6crVVqr9zGRJUpwIDAQAB"
  ttl     = 1800
}

resource "cloudflare_record" "conte_com_mg_tracking" {
  zone_id = cloudflare_zone.conte_com.id
  name    = "email.mg"
  type    = "CNAME"
  ttl     = 1
  value   = "mailgun.org"
}

resource "cloudflare_record" "conte_com_cityofgreen" {
  zone_id = cloudflare_zone.conte_com.id
  name    = "cityofgreen"
  type    = "AAAA"
  ttl     = 1
  proxied = true
  value   = "100::"
}
