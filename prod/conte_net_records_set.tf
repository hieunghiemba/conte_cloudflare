resource "cloudflare_record" "conte_net_chat" {
  zone_id = cloudflare_zone.conte_net.id
  name    = "chat"
  type    = "A"
  ttl     = 1
  proxied = true
  value   = "167.114.145.111"
}

resource "cloudflare_record" "conte_net_cloud" {
  zone_id = cloudflare_zone.conte_net.id
  name    = "cloud"
  type    = "A"
  ttl     = 1
  proxied = false
  value   = "51.79.71.170"
}

resource "cloudflare_zone_settings_override" "conte_net" {
  zone_id = cloudflare_zone.conte_net.id

  settings {
    ssl = "full"
  }
}
