# CHANGELOG

## 2021-05-10

- Confirmed that Cloudflare proxifying for Cloud was set to FALSE (avoid gateway timeout on large file transactioning)
